# ReadMe

**Project Structure/Decisions:**

When doing take home code challenges, sometimes developers tend to try to over impress which often leads to code that is more complicated than it needs to be. I decided to follow the KISS (*Keep it simple stupid*) methodology. This results in most of the code being easy for anyone to jump in and understand the flow or logic. I left small comments in the code explaining some decisions however I believe that self-documenting code is a good practice.

As the project left much to be decided by the developer, I decided to rely a bit less on storing dummy/mock JSON inside the app and opted for using an actual API. I am using the Mockaroo API to get dummy tweets to display to the user. I assign a random image to each tweet to simulate what Twitter normally looks like. The only other hard coded values in the app are the user login and password. The login details are currently set to: 

    User: kerde@test.com
    Password: test 

This means that authentication in the app is simply looking for a string match. This data is not encrypted however I did have the option to do so via the KeyChain, but I felt that would be unneeded for a simple project. 

**UI/Storyboards**

I decided to name the project MockingBird since Twitter uses a bird as their icon and the data used throughout this app comes from the Mockaroo API. I kept the UI relatively simple and created all of the views programmatically vs by use of Storyboards or xibs. I have no problem using Storyboards however in my experience, when projects begin to get very large, Storyboards become more of a problem than being useful. Using them requires good communication between developers to ensure that merge conflicts don't occur. I believe that you also have more control when you create views programmatically and through subclasses can make changes across the app much easier. I created helper extensions to simplify the use of auto-layout constraint methods. 

Pages:

The app consist of the following pages:

- Welcome/Splash Screen
- Login Screen
- Tweet Screen
- Submit Tweet Screen
- Menu ( option to logout user )

Assets:

Images and assets in the project were created by myself in Adobe XD.

---

**Network Layer**

A lot of applications use AlamoFire or other networking libraries for either making network calls or parsing data. I general opt for using URLSession for making my requests and for parsing of JSON , the use of Decodable. A single method takes in the request *WebRequest* object which contains any parameters, http method and url. This is basically similar to what libraries like AlamoFire do for the user behind the scenes. 

I used a simple strategy pattern to ensure that every request that is made follows a specific pattern of returning a response enum ( success/fail ) and a *RequestedObject*. The *RequestedObject* is generic so the value would change based on what request was made. This is set when we conform to the protocol.

    protocol APISourceStrategy {
    
        associatedtype RequestedObject
        var source: URL { get }
        func perform(completion: @escaping (_ status: APIResponseStatus, _ result: RequestedObject) -> ())
    }

---

**ViewModel**

For this project I used a simple MVVM pattern. All of the logic related to where the data comes from, how it is retrieved and how it is modified is taken care of by the viewModel. ViewControllers don't need to know about the data, they just need to know how to instruct the UI to behave based on said data. A ViewController must conform to the ViewModelCompatible protocol and define who their viewModel is. If a UITableView exists in this UIViewController, then the viewModel defined also conforms to the  UITableViewModel protocol which conforms to UITableViewDataSource. 

    protocol ViewModelCompatible {
        
        associatedtype ViewModel
        var viewModel: ViewModel { get set }
    }
    
    class ExampleViewController: ViewModelCompatible { 
    		typealias ViewModel = <#type#>
    }

    protocol UITableViewModel: UITableViewDataSource {
        
        associatedtype CellData
        
        var items: [ReusableCellConfigurator] { get set }
        
        var sections: Int { get set }    
    }

**Generics**

To expand on whats' mentioned previously about tableViews, when working with UITableViews, I typically use some helper extensions which reduce the general boilerplate code that is needed when creating a table. I do this so that all of the logic such as number of sections, what data is passed to the cells and why type of cells is taken care of by the viewModel for that particular UIViewController. 

    protocol UITableViewModel: UITableViewDataSource {
        
        associatedtype CellData
        
        var items: [ReusableCellConfigurator] { get set }
        
        var sections: Int { get set }    
    }
    
    protocol ReuseableCell {
        
        associatedtype Data
        func configure(withData data: Data)
    }
    
    protocol ReusableCellConfigurator {
        
        var cellReuseID: String { get }
        var height: CGFloat { get set }
        func configure(cell: UITableViewCell)
        func cell()->UITableViewCell.Type
    }
    
    class TableCellConfigurator<CellType: ReuseableCell, DataType> :
    ReusableCellConfigurator where CellType.Data == DataType, CellType: UITableViewCell {
        
        var item: DataType
        var height: CGFloat
        var cellReuseID: String
        
        init(item: DataType, height: CGFloat = UITableView.automaticDimension) {
            
            self.item = item
            self.height = height
            self.cellReuseID = String(describing: CellType.self)
        }
        
        func configure(cell: UITableViewCell) {
            (cell as? CellType)?.configure(withData: item)
        }
            
        func cell()->UITableViewCell.Type {
            return CellType.self
        }
    }

When a UITableViewCell conforms to the ReusableCell they define their data type.

    class MyCell: UITableViewCell, ReuseableCell{
        
        func configure(withData data: DATAType) {
            textLabel?.text = "\(data.value)\n\n"
        }
    }

---

**Unit/UI tests**

I was not sure how in depth to go with the unit testing as most of the logic in the app is simple and during the interview was mentioned that a normal amount is acceptable. I added tests for the parts that generally determine error handling and the flow of the app such as which UI to display to the user, validation on email, and login. The UI tests look for certain pop ups when the user enters invalid details and looks for the "submit" tweet button when valid login occurs.

---

**Dependencies:** 

I used CocoaPods to include Realm as well as SVProgressHud. There are multiple options for persisting data in your app however I generally rely on Realm over something like Core Data as Realm has better performance and has little boilerplate code. For initializing my dependancies I create a dependency extension and helper class that reduces the code that is present in the ApplicationDelegate. Often times I come across code that has 5 to 10 dependencies and the *didFinishLaunchingWithOptions* method becomes rather large with setup code and just looks messy.
import XCTest
@testable import Mockingbird
import RealmSwift
import Foundation

class MockingBirdTests: XCTestCase {

    var welcomeViewModel: WelcomeViewModel!
    var loginViewModel: LoginViewModel!
    var email: MockingBirdTextField!
    var password: MockingBirdTextField!
    
    override func setUp() {
        welcomeViewModel = WelcomeViewModel()
        loginViewModel = LoginViewModel()
        email = MockingBirdTextField()
        password = MockingBirdTextField()
    }
    
    override func tearDown() {
      welcomeViewModel = nil
      loginViewModel = nil
      MockingBirdDBHelper.deleteAll()
    }
    
    func testShowWelcomeScreenPositive(){
        XCTAssertEqual(welcomeViewModel.shouldShowWelcomeScreen, true)
    }
    
    func testShowWelcomeScreenNegative(){
        MockingBirdDBHelper.createUser()
        XCTAssertEqual(welcomeViewModel.shouldShowWelcomeScreen, false)
    }

    func testUserLoginPositive(){
        MockingBirdDBHelper.deleteAll()
        MockingBirdDBHelper.createUser()
        XCTAssertEqual(MockingBirdDBHelper.isUserLoggedIn(), true)
    }
 
    func testLogout(){
        MockingBirdDBHelper.createUser()
        MockingBirdDBHelper.deleteAll()
        XCTAssertEqual(MockingBirdDBHelper.isUserLoggedIn(), false)
    }
    
    func testInvalidEmail(){
        email.text = "kerde@"
        XCTAssertEqual(email.isValidEmail, false)
        email.text = "kerde@live"
        XCTAssertEqual(email.isValidEmail, false)
        email.text = "kerde"
        XCTAssertEqual(email.isValidEmail, false)
    }
    
    func testValidEmail(){
        email.text = "kerde@live.com"
        XCTAssertEqual(email.isValidEmail, true)
        email.text = "kerde@gmail.com"
        XCTAssertEqual(email.isValidEmail, true)
    }
    
    func testInvalidCredentials(){
        let username = "kerd@live.com"
        XCTAssertNotEqual(MockingBirdDBHelper.dummyUser().username, username)
    }
    
    func testValidCredentials(){
        let username = "kerde@test.com"
        XCTAssertEqual(MockingBirdDBHelper.dummyUser().username, username)
    }
    
    func testMissingDetails(){
        email.text = nil
        password.text = nil
        let result = loginViewModel.isMissingDetails(email: email, password: password)
        XCTAssertEqual(result, true)
    }
    
    func testNotMissingDetails(){
        email.text = "kerde@live.com"
        password.text = "1234"
        let result = loginViewModel.isMissingDetails(email: email, password: password)
        XCTAssertEqual(result, false)
    }
}

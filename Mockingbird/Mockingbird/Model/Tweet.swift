import Foundation
import RealmSwift

/*
 
 Made Decodable so that we can parse our objects straight from
 URLSession data , into our Realm object

*/

class Tweet: Object, Decodable {
    
    @objc dynamic var title: String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var summary: String = ""
    @objc dynamic var image: String = UIImage.getFakeImage()
    @objc dynamic var username: String = ""
    @objc dynamic var timeStamp = Date()

    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case summary = "summary"
        case username = "username"
    }
    
    public convenience init(from userTweet:String){
        self.init()
        
        id = String.getUniqueTweetId()
        let user = MockingBirdDBHelper.loggedInUser()
        title = user.name
        summary = userTweet
        image = user.image
        username = user.username
    }
    
    public required convenience init(from decoder: Decoder) throws {
        
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id) ?? "0"
        title = try values.decodeIfPresent(String.self, forKey: .title) ?? ""
        summary = try values.decodeIfPresent(String.self, forKey: .summary) ?? ""
        username = try values.decodeIfPresent(String.self, forKey: .username) ?? ""
    }
}

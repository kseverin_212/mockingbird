import Foundation
import RealmSwift

/*
This class deals with all Database related tasks
ViewControllers should not have any knowledge of where the data comes from
This makes it easy to switch database e.g if we wanted to use Core Data now instead of Realm
Our view controllers would stay the same but our MockingBirdDBHelper helper would change
*/

final class MockingBirdDBHelper {
    
    class func dummyUser()->(username:String, password:String){
        return ("kerde@test.com", "test")
    }
    
    class func createUser(){
        let user = User()
        user.firstName = "Kerde"
        user.lastName = "Severin"
        user.email = "kerde@test.com"
        user.profileImg = "kerde"
        MockingBirdDBHelper.save(user)
    }
    
    class func isUserLoggedIn()->Bool {
        guard let _ = try! Realm().objects(User.self).first else { return false }
        return true
    }
    
    class func loggedInUser()->(image:String, username: String, name:String){
    
        guard let user = try! Realm().objects(User.self).first else { return ("","","") }
        return (user.profileImg, user.email, String.makeFullName(from: user.firstName, lastName: user.lastName) )
    }
    
    class func getAllTweets()->[Tweet]{
        let result = try! Realm().objects(Tweet.self).sorted(by: { $0.timeStamp > $1.timeStamp})
        return result
    }
    
    class func save(_ thisObject:Object){
        let realm = try! Realm()
        try! realm.write {
            realm.add((thisObject), update: true)
        }
    }
    
    class func deleteAll(){
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    class func logout(){
        deleteAll()
    }
}

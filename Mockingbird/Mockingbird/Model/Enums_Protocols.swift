import Foundation

protocol TextFieldInputValidation {
    
    var isEmpty:Bool { get }
    var isValidEmail:Bool { get }
}

protocol APISourceStrategy {

    associatedtype RequestedObject
    var source: URL { get }
    func perform(completion: @escaping (_ status: APIResponseStatus, _ result: RequestedObject) -> ())
}

protocol UITableViewMenuSelectDelegate {
    func menu(didSelectRowAt indexPath: IndexPath)
}

public enum MenuState: Int {
    case isOpen = 0, isClosed
}

public enum APIResponseStatus: Int {
    case Success = 0, Fail
}

//There are multiple other types such as PUT, but not used for this project
public enum HTTPMethod: String {
    case GET  = "GET"
    case POST = "POST"
}

public enum MenuOption: String {
    
    case Logout = "Logout"
    case About = "About"
    case Close = "Close"
    
    var value:String {
       return self.rawValue
    }
}

extension MenuOption: CaseIterable {}


public enum LoginResult {
    case Success
    case InvalidEmail
    case MissingDetails
    case InvalidDetails
}

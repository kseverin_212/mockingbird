import Foundation
import RealmSwift

class User: Object{
    
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var profileImg: String = UIImage.defaultAppImage()

    let tweets = List<Tweet>()
    
    override static func primaryKey() -> String? {
        return "email"
    }
}

import Foundation
import UIKit

class TweetListViewModel: NSObject, UITableViewModel {
    
    typealias TweetCellConfigurator = TableCellConfigurator<TweetDetailsTableViewCell, Tweet>
    
    typealias CellData = [Tweet]
    
    var items = [ReusableCellConfigurator]()
    
    var footerView = UIView()
    
    var sections: Int = 1
    
    override init() {
        super.init()
    }
    
    func updateModelData(data:[Tweet]?) {
        
        guard let newTweets = data else { return }
        for item in newTweets{
            MockingBirdDBHelper.save(item)
        }
        refreshData()
    }
    
    func refreshData(){
        items.removeAll()
        let tweets = MockingBirdDBHelper.getAllTweets()
        for item in tweets {
            self.items.append(TweetCellConfigurator(item: item))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: item.cellReuseID) else {
            return UITableViewCell() }
        item.configure(cell: cell)
        return cell
    }
    
    func refreshTweets(result: @escaping (APIResponseStatus)->Void){
        
        TweetService().perform { [weak self] (status, tweets ) in
            
            guard status != .Fail else {
                return result(.Fail)
            }
            DispatchQueue.main.async {
                self?.updateModelData(data: tweets)
            }
            return result(.Success)
        }
    }
}

import Foundation

class WelcomeViewModel {
        
    lazy var shouldShowWelcomeScreen: Bool = {
        return !MockingBirdDBHelper.isUserLoggedIn()
    }()
}

import Foundation
import UIKit

class MenuViewModel: NSObject, UITableViewModel {
    
    typealias MenuTextCellConfigurator = TableCellConfigurator<MenuCell, MenuOption>
    
    typealias CellData = [MenuOption]
    
    var items = [ReusableCellConfigurator]()
    
    var sections: Int = 1
    
    var footerView = UIView()
    
    override init() {
        super.init()
        updateModelWithData(data: MenuOption.allCases)
    }
    
    func updateModelWithData(data: [MenuOption]) {
        for item in data {
            items.append(MenuTextCellConfigurator(item: item))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: item.cellReuseID) else {
            return UITableViewCell() }
        item.configure(cell: cell)
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return cell
    }
}

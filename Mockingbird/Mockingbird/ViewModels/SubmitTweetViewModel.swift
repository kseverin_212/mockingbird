import Foundation
import UIKit

class SubmitTweetViewModel {
    
    let tweetMaxLength = 100
    
    func profileImage()->UIImage{
        guard let image = UIImage(named: MockingBirdDBHelper.loggedInUser().image) else { return UIImage() }
        return image
    }
    
    func submitTweet(_ message:String){
        let thisTweet =  Tweet(from: message)
        MockingBirdDBHelper.save(thisTweet)
    }
}

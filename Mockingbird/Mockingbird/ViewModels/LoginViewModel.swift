import Foundation
import UIKit

class LoginViewModel {
    
    func isMissingDetails(email:MockingBirdTextField, password:MockingBirdTextField)->Bool{
        
        guard !email.isEmpty, !password.isEmpty else {
            return true
        }
        return false
    }
    
    func validateInput(emailTxtField:MockingBirdTextField, passwordTxtField:MockingBirdTextField, loginResult: @escaping (LoginResult)->Void){
        
        guard !isMissingDetails(email: emailTxtField, password: passwordTxtField) else {
            return loginResult(.MissingDetails)
        }
        
        guard emailTxtField.isValidEmail else {
            return loginResult(.InvalidEmail)
        }
        
        let loginService = LoginService(username: emailTxtField.text!, password: passwordTxtField.text!)
        loginService.perform { (result, _) in
            
            guard result == .Success else {
                return loginResult(.InvalidDetails)
            }
            
            return loginResult(.Success)
        }
    }
}

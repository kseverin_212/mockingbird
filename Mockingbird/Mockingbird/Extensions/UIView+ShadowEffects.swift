import Foundation
import UIKit

extension UIView {
    
    func addShadow(color:UIColor = .mbGreen){
        layer.shadowColor = color.cgColor
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.5
        addSimpleBorder()
    }
    
    func addSimpleBorder(color:UIColor = .mbLightGrey){
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.2
    }
}

import Foundation

extension String {
    
    func isEmailFormat()->Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    static func makeFullName(from firstName:String, lastName:String)->String{
        return "\(firstName) \(lastName)"
    }
    
    static func getUniqueTweetId()->String{
        return UUID().description
    }
}

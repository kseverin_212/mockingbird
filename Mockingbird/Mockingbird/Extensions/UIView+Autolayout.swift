import Foundation
import UIKit

extension UIViewController{
    
    func activate(_ constraints: [NSLayoutConstraint]) {
        NSLayoutConstraint.activate(constraints)
    }
    
    func pinToView(_ view: UIView, constant:CGFloat = 0){
        self.view.addSubview(view)
        view.pinToSuperview(constant: constant)
    }
}

extension UIView {
    
    func activate(_ constraints: [NSLayoutConstraint]) {
        NSLayoutConstraint.activate(constraints)
    }
    
    func enableAutoLayout() {
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    func pinToSuperview() {
        
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            
            activate([
                
                leadingAnchor.constraint(equalTo: superview.leadingAnchor),
                trailingAnchor.constraint(equalTo: superview.trailingAnchor),
                topAnchor.constraint(equalTo: superview.topAnchor),
                bottomAnchor.constraint(equalTo: superview.bottomAnchor)
                ])
        }
    }
    
    func pinToSuperview(withTopPadding constant:CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let superview = superview {
            
            NSLayoutConstraint.activate([
                leadingAnchor.constraint(equalTo: superview.leadingAnchor),
                trailingAnchor.constraint(equalTo: superview.trailingAnchor),
                topAnchor.constraint(equalTo: superview.topAnchor,constant: constant),
                bottomAnchor.constraint(equalTo: superview.bottomAnchor)
                ])
        }
    }
    
    func setEqualHeightAndWidth(constant: CGFloat){
        
        translatesAutoresizingMaskIntoConstraints = false
        activate([
            heightAnchor.constraint(equalToConstant: constant),
            widthAnchor.constraint(equalToConstant: constant)
            ])
    }
    
    func setHeight(constant: CGFloat){
        
        translatesAutoresizingMaskIntoConstraints = false
        activate([
            heightAnchor.constraint(equalToConstant: constant)
            ])
    }
    
    func setWidth(constant: CGFloat){
        
        translatesAutoresizingMaskIntoConstraints = false
        activate([
            widthAnchor.constraint(equalToConstant: constant)
            ])
    }
    
    func pinToSuperview(withLeading constant: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            
            activate([
                
                leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: constant),
                trailingAnchor.constraint(equalTo: superview.trailingAnchor),
                topAnchor.constraint(equalTo: superview.topAnchor),
                bottomAnchor.constraint(equalTo: superview.bottomAnchor)
                ])
        }
    }
    
    func pinToSuperview(constant: CGFloat = 0) {
        
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            
            activate([
                
                leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: constant),
                trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -constant),
                topAnchor.constraint(equalTo: superview.topAnchor, constant: constant),
                bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -constant)
                ])
        }
    }
    
    func pinLeadingAndTrailingToSuperview(constant: CGFloat = 0) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let superview = superview {
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: constant).isActive = true
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -constant).isActive = true
        }
    }
}

import Foundation
import UIKit

extension UIColor{
    
    static let mbLightGrey = UIColor(red:0.95, green:0.95, blue:0.99, alpha:1.0)
    static let mbDarkGrey =  UIColor(red:0.19, green:0.19, blue:0.19, alpha:1.0)
    static let mbGreen = UIColor(red:0.16, green:0.93, blue:0.88, alpha:1.0)
    static let mbSplashGrey = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
}

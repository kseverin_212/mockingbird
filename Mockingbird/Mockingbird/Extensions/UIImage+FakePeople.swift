import Foundation
import UIKit

//Images were pulled from ThisPersonDoesNotExist.com.
//Chose 7 images which will be used randomly to assign to the users who posted tweets
//Normally this data would come from the API. Mockaroo API does not provide images in their api
extension UIImage{
    
    static let fakePeople:[String] = Array(1...7).compactMap({"\($0).jpeg"})
    
    static func getFakeImage()->String{
        guard
            let random = UIImage.fakePeople.randomElement() else { return "mocking" }
        return random
    }
    
    static func defaultAppImage()->String{
        return "mocking"
    }
}

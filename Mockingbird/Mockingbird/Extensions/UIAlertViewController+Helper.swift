import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(withTitle title: String, message: String, closeAction: @escaping () -> Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .default) { (_:UIAlertAction) in
            closeAction()
        }
        alert.addAction(action)
        alert.show()
    }
    
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func pop() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UIAlertController {
    
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let viewCtrl = UIViewController()
        viewCtrl.view.backgroundColor = .clear
        win.rootViewController = viewCtrl
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        viewCtrl.present(self, animated: true, completion: nil)
    }
}

import Foundation
import UIKit

extension UINavigationBar{
    
    static func customizeWithTheme(){
        UINavigationBar.appearance().barTintColor = .mbDarkGrey
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = .mbGreen
        let textAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.mainFont(ofSize: 15)
        ]        
        UINavigationBar.appearance().titleTextAttributes = textAttributes
    }
}

import Foundation
import UIKit
import SVProgressHUD
import RealmSwift

protocol Dependency {
    func execute()
}

struct InitializeLoadingHud : Dependency {
    func execute() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.black)
    }
}

struct InitializeDatabase : Dependency {
    func execute() {
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {}
        })
        Realm.Configuration.defaultConfiguration = config
    }
}


final class DependencyManager {
    
    func build() -> [Dependency] {
        return [
            InitializeLoadingHud(),
            InitializeDatabase()
        ]
    }
}

import Foundation
import UIKit

protocol ViewModelCompatible {
    
    associatedtype ViewModel
    var viewModel: ViewModel { get set }
}

protocol UITableViewModel: UITableViewDataSource {
    
    associatedtype CellData
    
    var items: [ReusableCellConfigurator] { get set }
    
    var sections: Int { get set }    
}

protocol ReuseableCell {
    
    associatedtype Data
    func configure(withData data: Data)
}

protocol ReusableCellConfigurator {
    
    var cellReuseID: String { get }
    var height: CGFloat { get set }
    func configure(cell: UITableViewCell)
    func cell()->UITableViewCell.Type
}

class TableCellConfigurator<CellType: ReuseableCell, DataType> :
ReusableCellConfigurator where CellType.Data == DataType, CellType: UITableViewCell {
    
    var item: DataType
    var height: CGFloat
    var cellReuseID: String
    
    init(item: DataType, height: CGFloat = UITableView.automaticDimension) {
        
        self.item = item
        self.height = height
        self.cellReuseID = String(describing: CellType.self)
    }
    
    func configure(cell: UITableViewCell) {
        (cell as? CellType)?.configure(withData: item)
    }
        
    func cell()->UITableViewCell.Type {
        return CellType.self
    }
}

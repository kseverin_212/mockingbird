import Foundation
import UIKit

class MenuButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupAppearance()
    }
    
    func setupAppearance() {
        setImage(#imageLiteral(resourceName: "Settings"), for: .normal)
        imageView?.contentMode = .scaleAspectFit
    }
}

import Foundation
import UIKit

class ComposeTweetButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupAppearance()
    }
    
    func setupAppearance() {
        setImage(#imageLiteral(resourceName: "add"), for: .normal)
        imageView?.contentMode = .scaleAspectFit
        addShadow()
    }
}

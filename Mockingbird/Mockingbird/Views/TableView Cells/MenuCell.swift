
import Foundation
import UIKit

class MenuCell: UITableViewCell, ReuseableCell{
    
    func configure(withData data: MenuOption) {
        textLabel?.text = "\(data.value)\n\n"
    }
}

import UIKit

class TweetDetailsTableViewCell: UITableViewCell, ReuseableCell {
    
    lazy var container: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .top
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.enableAutoLayout()
        stackView.spacing = 10
        return stackView
    }()

    lazy var profileImgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.enableAutoLayout()
        img.backgroundColor = .mbGreen
        img.layer.cornerRadius = 30
        img.layer.masksToBounds = true
        return img
    }()
    
    lazy var subContainer: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.enableAutoLayout()
        stackView.spacing = 10
        return stackView
    }()
    
    lazy var postUsernameTitlbeLbl: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.enableAutoLayout()
        return label
    }()
    
    lazy var postTitleLbl: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.enableAutoLayout()
        return label
    }()
    
    lazy var postSummaryLbl: UITextView = {
        
        let textView = UITextView()
        textView.enableAutoLayout()
        textView.textAlignment = .left
        textView.isSelectable = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){

        addSubview(container)
        container.addArrangedSubview(profileImgView)
        container.addArrangedSubview(subContainer)
        container.pinToSuperview(constant: 10)
        subContainer.addArrangedSubview(postUsernameTitlbeLbl)
        subContainer.addArrangedSubview(postTitleLbl)
        subContainer.addArrangedSubview(postSummaryLbl)
        profileImgView.setEqualHeightAndWidth(constant: 60)
    }
    
    func configure(withData data: Tweet) {
     
        profileImgView.image = UIImage(named: data.image)
        postTitleLbl.text = data.title
        postSummaryLbl.text = data.summary
        postUsernameTitlbeLbl.text = "@\(data.username)"
    }
}

import Foundation
import UIKit

class PrimaryActionButton: UIButton {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupAppearance()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAppearance()
    }
    
    func setupAppearance(){
        backgroundColor = .clear
        setTitleColor(.white, for: .normal)
        setBackgroundImage(#imageLiteral(resourceName: "greenbutton"), for: .normal)
        addShadow()
    }
}

import Foundation
import UIKit

class MockingBirdTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAppearance()
        setAttributes()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAppearance()
        setAttributes()
    }
    
    func setAttributes(){
        autocorrectionType = .no
        spellCheckingType = .no
        autocapitalizationType = .none
        isAccessibilityElement = false
    }
    
    func setupAppearance(){
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        leftView = padding
        leftViewMode = .always
        spellCheckingType = .no
        autocorrectionType = .no
        borderStyle = .none
        font = UIFont.systemFont(ofSize: 15)
        layer.cornerRadius = 5
        layer.borderWidth = 0.75
        layer.borderColor = UIColor.mbDarkGrey.cgColor
    }
}

extension MockingBirdTextField : TextFieldInputValidation{
    
    var isEmpty: Bool {
        guard let status = text?.isEmpty else { return true }
        return status
    }
    
    var isValidEmail:Bool {
        get{
            guard let text = text else { return false }
            return text.isEmailFormat()
        }
    }
}

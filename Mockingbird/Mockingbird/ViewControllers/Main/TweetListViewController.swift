import Foundation
import UIKit

class TweetListViewController: UIViewController{

    var viewModel = TweetListViewModel()

    lazy var tweetButton: ComposeTweetButton = {
        let btn = ComposeTweetButton()
        btn.enableAutoLayout()
        btn.addTarget(self,
                      action: #selector(TweetListViewController.newTweetBtnTapped),
                      for: .touchUpInside)
        return btn
    }()
    
    lazy var tweetTblView: UITableView = {
        let table = UITableView()
        table.delegate = self
        table.showsVerticalScrollIndicator = false
        table.enableAutoLayout()
        return table
    }()
    
    lazy var refresherView: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self,
                            action: #selector(TweetListViewController.refreshTweets),
                            for: .valueChanged)
        return refresher
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Main"
        setupViews()
        setViewModel()
        view.backgroundColor = .mbLightGrey
        tweetTblView.tableFooterView = viewModel.footerView
        refreshTweets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshData()
    }
    
    func setupViews(){
        
        pinToView(tweetTblView,constant: 10)
        view.addSubview(tweetButton)
        
        tweetButton.setEqualHeightAndWidth(constant: 60)
        tweetTblView.refreshControl = refresherView
        
        activate([
            tweetButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30),
            tweetButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            ])
    }
    
    func setViewModel(){
        tweetTblView.dataSource = viewModel
        registerCells()
    }
    
    func registerCells(){
        viewModel.items.forEach({
            tweetTblView.register($0.cell(), forCellReuseIdentifier: $0.cellReuseID)
        })
    }
    
    func refreshData(){
        if !viewModel.items.isEmpty{ registerCells() }
        viewModel.refreshData()
        refresherView.endRefreshing()
        tweetTblView.reloadData()
    }
    
    @objc func refreshTweets(){
        
        viewModel.refreshTweets { [weak self] (result) in
           
            switch result{
            case .Fail:
                self?.showAlert(withTitle: "Ooops", message: "Looks like we could not complete your request") {}
            default:
                DispatchQueue.main.async {
                    self?.refreshData()
                }
            }
        }
    }
    
    @objc func newTweetBtnTapped(){
        let submTweetVC = UINavigationController(rootViewController:SubmitTweetViewController())
        present(submTweetVC, animated: true, completion: nil)
    }
}

extension TweetListViewController : ViewModelCompatible {}

extension TweetListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

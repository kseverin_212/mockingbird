import Foundation
import UIKit

class SlideDownViewController: UIViewController {
    
    var menuState:MenuState = .isClosed
    
    var navigationViewController: UINavigationController!
    
    var mainViewController:UIViewController! {
        didSet{
            addMenuButton()
        }
    }
    
    var menuButton: UIBarButtonItem = {
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "Settings").withRenderingMode(.alwaysOriginal),style: .plain,
                                                                               target: self,
                                                                               action: #selector(SlideDownViewController.toggleMenu))
        return btn
    }()
    
    lazy var menuViewController:MenuViewContorller = {
        let menu = MenuViewContorller()
        menu.delegate = self
        menu.view.enableAutoLayout()
        return menu
    }()
    
    lazy var scrollView:UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        scrollView.enableAutoLayout()
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    lazy var controllerStackView: UIStackView={
        let stackView = UIStackView()
        stackView.enableAutoLayout()
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 0
        stackView.axis = .vertical
        stackView.backgroundColor = .white
        return stackView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        assert(false, "init(rootViewController:UIViewController) must be used")
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        assert(false, "init(rootViewController:UIViewController) must be used")
        super.init(nibName: nil, bundle: nil)
    }
    
    init(rootViewController:UIViewController){
        
        super.init(nibName: nil, bundle: nil)
        navigationViewController = UINavigationController()
        setRoot(to: rootViewController)
        menuViewController.delegate = self
        setupViews()
        setupConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        closeMenu(isAnimated: false)
    }
    
    func setupViews(){
        view.addSubview(scrollView)
        scrollView.addSubview(controllerStackView)
        controllerStackView.addArrangedSubview(menuViewController.view)
        addChild(menuViewController)
        menuViewController.didMove(toParent: self)
        navigationViewController.view.enableAutoLayout()
        controllerStackView.addArrangedSubview(navigationViewController.view)
        addChild(navigationViewController)
        navigationViewController.didMove(toParent: self)
    }
    
    func setupConstraints(){
        scrollView.pinToSuperview()
        controllerStackView.pinToSuperview()
        activate([
            controllerStackView.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 2),
            controllerStackView.widthAnchor.constraint(equalTo: view.widthAnchor)
            ])
    }
    
    func addMenuButton(){
        guard let _ = mainViewController else { return }
        mainViewController.navigationItem.rightBarButtonItem = menuButton
    }
    
    @objc func toggleMenu(){
        menuState = menuState == .isClosed ? .isOpen : .isClosed
        adjustMenuBasedOnState(state: menuState)
    }
    
    func adjustMenuBasedOnState(state:MenuState, animated:Bool = true){
        let scrollOffset = state == .isClosed ? scrollView.frame.maxY : 80
        let adjustment = CGPoint(x: 0, y: scrollOffset)
        scrollView.setContentOffset(adjustment, animated: animated)
    }
    
    func closeMenu(isAnimated:Bool = true){
        adjustMenuBasedOnState(state: .isClosed,animated: isAnimated)
    }
    
    func openMenu(isAnimated:Bool = true){
        adjustMenuBasedOnState(state: .isOpen,animated: isAnimated)
    }
    
    private func setRoot(to viewController:UIViewController){
        
        if let _ = mainViewController{
            mainViewController.removeFromParent()
            mainViewController = nil
        }
        mainViewController = viewController
        navigationViewController.setViewControllers([mainViewController], animated: true)
    }
}

extension SlideDownViewController : UITableViewMenuSelectDelegate{
    
    func menu(didSelectRowAt indexPath: IndexPath) {
        
        let selected = (MenuOption.allCases.map { $0 })[indexPath.row]
        
        switch selected {
        case .Logout:
            MockingBirdDBHelper.logout()
            dismiss()
        case .About:
        showAlert(withTitle: "Hello", message: "Created by Kerde Severin") {}
        default:
            toggleMenu()
        }
    }
}


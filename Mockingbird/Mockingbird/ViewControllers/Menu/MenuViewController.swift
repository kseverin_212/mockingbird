import Foundation
import UIKit

class MenuViewContorller : UIViewController {
    
    var delegate:UITableViewMenuSelectDelegate?
    
    var viewModel = MenuViewModel()
    
    lazy var menuTbl : UITableView = {
        
        let table = UITableView()
        table.delegate = self
        table.allowsMultipleSelection = false
        table.backgroundColor = .mbDarkGrey
        table.separatorColor = .clear
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .mbDarkGrey
        
        menuTbl.dataSource = viewModel
        
        viewModel.items.forEach({
            menuTbl.register($0.cell(), forCellReuseIdentifier: $0.cellReuseID)
        })
        setupViews()
    }
    
    func setupViews(){
        view.addSubview(menuTbl)
        menuTbl.pinToSuperview(withTopPadding: 200)
    }
}

extension MenuViewContorller: ViewModelCompatible{}

extension MenuViewContorller : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.menu(didSelectRowAt: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

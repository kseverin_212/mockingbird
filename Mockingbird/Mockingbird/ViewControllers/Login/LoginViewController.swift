import Foundation
import UIKit

class LoginViewController: UIViewController{
    
    var viewModel = LoginViewModel()
    
    lazy var titleTextView: UITextView = {
        let textView = UITextView()
        var attributedText = NSMutableAttributedString(string: "Login to MockingBird",
                                                       attributes: [NSAttributedString.Key.font:
                                                        UIFont.mainFont(ofSize: 20)])
        textView.attributedText = attributedText
        textView.enableAutoLayout()
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        return textView
    }()
    
    lazy var splashGradientBackground: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.backgroundColor = .clear
        img.image = #imageLiteral(resourceName: "background_pattern")
        img.enableAutoLayout()
        return img
    }()
    
    lazy var logoImgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = #imageLiteral(resourceName: "mocking")
        img.enableAutoLayout()
        return img
    }()
    
    lazy var container: UIStackView = {
        let stck = UIStackView()
        stck.alignment = .fill
        stck.distribution = .fillEqually
        stck.axis = .vertical
        stck.enableAutoLayout()
        stck.spacing = 15
        return stck
    }()
    
    lazy var usernameTxtField: MockingBirdTextField = {
        let mbTextField = MockingBirdTextField()
        mbTextField.enableAutoLayout()
        mbTextField.placeholder = "Email Address"
        return mbTextField
    }()
    
    lazy var passwordTxtField: MockingBirdTextField = {
        let mbTextField = MockingBirdTextField()
        mbTextField.enableAutoLayout()
        mbTextField.isSecureTextEntry = true
        mbTextField.placeholder = "Password"
        return mbTextField
    }()
    
    lazy var loginBtn: PrimaryActionButton = {
        let btn = PrimaryActionButton()
        btn.setTitle("Login", for: .normal)
        btn.addTarget(self, action:#selector(LoginViewController.attempLogin),
                      for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        title = "Login"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setupViews(){
        
        view.backgroundColor = .white
        view.addSubview(splashGradientBackground)
        splashGradientBackground.pinToSuperview()
        container.addArrangedSubview(titleTextView)
        container.addArrangedSubview(logoImgView)
        container.addArrangedSubview(usernameTxtField)
        container.addArrangedSubview(passwordTxtField)
        container.addArrangedSubview(loginBtn)
        view.addSubview(container)
        container.pinLeadingAndTrailingToSuperview(constant: 20)
        activate([
            container.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            container.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor, multiplier: 0.4)
            ])
    }
    
    @objc func attempLogin(){
        
        viewModel.validateInput(emailTxtField: usernameTxtField, passwordTxtField: passwordTxtField) { [weak self] (loginResult) in
            
            switch loginResult{
            case .InvalidDetails:
                self?.showAlert(withTitle: "Hmm", message: "You've entered the wrong username and password") {}
            case .InvalidEmail:
                self?.showAlert(withTitle: "Oops", message: "Please enter a correct email address") {}
            case .MissingDetails:
                self?.showAlert(withTitle: "Woah", message: "Looks like you forget to input some data") {}
            default:
                self?.loadTweetView()
            }
        }
    }
    
    func loadTweetView(){
        let tweetVC = SlideDownViewController(rootViewController:TweetListViewController())
        present(tweetVC, animated: true, completion: nil)
    }
}

extension LoginViewController: ViewModelCompatible{}

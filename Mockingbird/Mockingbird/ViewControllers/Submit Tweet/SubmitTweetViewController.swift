import Foundation
import UIKit

class SubmitTweetViewController: UIViewController{
    
    var viewModel = SubmitTweetViewModel()
    
    lazy var userTweetTxtView: UITextView = {
       let txtView = UITextView()
        txtView.isScrollEnabled = false
        txtView.enableAutoLayout()
        txtView.delegate = self
        txtView.showsVerticalScrollIndicator = false
        txtView.font = UIFont.systemFont(ofSize: 18)
        txtView.addShadow()
        return txtView
    }()
    
    lazy var profileImgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.enableAutoLayout()
        img.backgroundColor = .mbGreen
        img.layer.cornerRadius = 30
        img.layer.masksToBounds = true
        img.backgroundColor = .clear
        return img
    }()
    
    lazy var titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Tell Us Whats Going On"
        lbl.numberOfLines = 1
        lbl.font = UIFont.boldSystemFont(ofSize: 18)
        lbl.textColor = .black
        return lbl
    }()
    
    lazy var container: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .top
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.enableAutoLayout()
        stackView.spacing = 10
        return stackView
    }()
    
    lazy var subContainer: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.enableAutoLayout()
        stackView.spacing = 10
        return stackView
    }()
    
    lazy var cancelBtn: UIBarButtonItem = {
        let btn = UIBarButtonItem(title: "Cancel",
                                  style: .plain,
                                  target: self,
                                  action: #selector(SubmitTweetViewController.cancelBtnTapped))
        btn.tintColor = .mbGreen
        return btn
    }()
    
    lazy var submitBtn: UIBarButtonItem = {
        let btn = UIBarButtonItem(title: "Tweet!",
                                  style: .plain,
                                  target: self,
                                  action: #selector(SubmitTweetViewController.submitBtnTapped))
        btn.isEnabled = false
        btn.tintColor = .mbGreen
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews(){
        
        title = "Compose"
        profileImgView.image = viewModel.profileImage()
        setNavigationButtons()
        subContainer.addArrangedSubview(profileImgView)
        subContainer.addArrangedSubview(titleLbl)
        container.addArrangedSubview(subContainer)
        container.addArrangedSubview(userTweetTxtView)
        view.addSubview(container)
        container.pinLeadingAndTrailingToSuperview(constant: 20)
        profileImgView.setEqualHeightAndWidth(constant: 60)
        activate([
            container.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            ])
        
        userTweetTxtView.becomeFirstResponder()
        view.backgroundColor = .lightGray
    }
    
    func setNavigationButtons(){
        navigationItem.leftBarButtonItem = cancelBtn
        navigationItem.rightBarButtonItem = submitBtn
        navigationController?.title = "Compose Tweet"
    }
    
    @objc func cancelBtnTapped(){
        dismiss()
    }
    
    @objc func submitBtnTapped(){
        viewModel.submitTweet(userTweetTxtView.text)
        dismiss()
    }
}

extension SubmitTweetViewController: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard textView.text.count > 0 else {
            submitBtn.isEnabled = false
            return
        }
        submitBtn.isEnabled = true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let userText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let currentChars = userText.count
        self.submitBtn.isEnabled = currentChars > 0 ? true : false
        let maxRange = currentChars < self.viewModel.tweetMaxLength
        if !maxRange {
            self.userTweetTxtView.shake()
        }
        return maxRange
    }
}

extension SubmitTweetViewController: ViewModelCompatible{}

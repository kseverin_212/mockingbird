import Foundation
import UIKit

class WelcomeViewController: UIViewController{
    
    var viewModel = WelcomeViewModel()
    
    lazy var loginTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Log in to your account"
        lbl.font = UIFont.mainFont(ofSize: 18)
        lbl.enableAutoLayout()
        lbl.backgroundColor = .clear
        lbl.textAlignment = .left
        lbl.isHidden = true
        return lbl
    }()
    
    lazy var subTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Connect to people\naround the world"
        lbl.font = UIFont.mainFont(ofSize: 28)
        lbl.enableAutoLayout()
        lbl.numberOfLines = 0
        lbl.textAlignment = .left
        lbl.textColor = .mbGreen
        lbl.backgroundColor = .clear
        lbl.isHidden = true
        return lbl
    }()
    
    lazy var termsLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Terms & Conditions"
        lbl.enableAutoLayout()
        lbl.textAlignment = .center
        lbl.font = UIFont.mainFont(ofSize: 8)
        lbl.backgroundColor = .clear
        lbl.isHidden = true
        return lbl
    }()
    
    lazy var splashLogoImgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.backgroundColor = .clear
        img.image = #imageLiteral(resourceName: "illustration")
        img.enableAutoLayout()
        return img
    }()
    
    lazy var loginBtn: PrimaryActionButton  = {
        let button = PrimaryActionButton()
        button.setTitle("Get Started", for: .normal)
        button.addTarget(self, action:#selector(WelcomeViewController.loadLoginView),
                         for: .touchUpInside)
        button.isHidden = true
        return button        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if viewModel.shouldShowWelcomeScreen{
            showWelcomeElements()
        }else{
            loadMainView()
        }
    }
    
    func setupViews() {
        view.addSubview(splashLogoImgView)
        view.addSubview(loginTitleLbl)
        view.addSubview(subTitleLbl)
        view.addSubview(loginBtn)
        view.addSubview(termsLbl)
        splashLogoImgView.pinLeadingAndTrailingToSuperview()
        loginTitleLbl.pinLeadingAndTrailingToSuperview(constant: 20)
        subTitleLbl.pinLeadingAndTrailingToSuperview(constant: 20)
        loginBtn.pinLeadingAndTrailingToSuperview(constant: 30)
        termsLbl.pinLeadingAndTrailingToSuperview(constant: 30)
        
        activate([
            splashLogoImgView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            splashLogoImgView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.4),
            loginTitleLbl.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            subTitleLbl.topAnchor.constraint(equalTo: loginTitleLbl.bottomAnchor, constant: 20),
            loginBtn.heightAnchor.constraint(equalToConstant: 44),
            termsLbl.topAnchor.constraint(equalTo: loginBtn.bottomAnchor, constant: 10),
            termsLbl.heightAnchor.constraint(equalToConstant: 20),
            termsLbl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            ])
    }
    
    func showWelcomeElements(){
        subTitleLbl.isHidden = false
        loginTitleLbl.isHidden = false
        loginBtn.isHidden = false
        termsLbl.isHidden = false
    }
    
    func loadMainView(){
        let mainVC = SlideDownViewController(rootViewController: TweetListViewController())
        present(mainVC, animated: true, completion: nil)
    }
    
    @objc func loadLoginView(){
        let loginVC = LoginViewController()
        show(loginVC, sender: self)
    }
}

extension WelcomeViewController: ViewModelCompatible{}

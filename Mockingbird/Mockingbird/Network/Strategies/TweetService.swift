import Foundation
import RealmSwift

public class TweetService: APISourceStrategy {
    
    //Using the Mockaroo API to get data since we don't have an actual service to hit
    var source: URL{
        let address = "https://api.mockaroo.com/api/779cd9e0?count=10&key=0f6b7b60"
        guard let url = URL(string: address) else { return URL(string:"")!}
        return url
    }
    
    func perform(completion: @escaping (APIResponseStatus, [Tweet]?) -> ()) {
        
        let request = WebRequest(url:source)
        
        MBNetworkClient().makeRequest(request) { (status, data) in
            
            guard let jsonData = data else { completion(.Fail,nil); return }
            
            do{
                let newTweets = try JSONDecoder().decode([Tweet].self, from: jsonData)
                completion(.Success, newTweets)
            }
            catch _ { completion(.Fail,nil) }
        }
    }
}

import Foundation
import UIKit

public class LoginService: APISourceStrategy {
    
    var user:String?
    
    var pass:String?
    
    var source: URL{
        let address = "https://dummyloginApi.net"
        guard let url = URL(string: address) else { return URL(string:"")!}
        return url
    }
    
    init(username:String, password:String) {
        self.user = username
        self.pass = password
    }
    
    func perform(completion: @escaping (APIResponseStatus, Bool) -> ()) {

        //Would normally be used to pass to request, mocking for now with 3 second delay
        guard let username = user, let password = pass else {
            completion(.Fail, false)
            return
        }
        
        MBNetworkClient.showLoadingHud()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            MBNetworkClient.hideLoadingHud()
            
            //Normally this would hit an api to validate, for now I am using dummy hard coded values to validate the user
            guard username == MockingBirdDBHelper.dummyUser().username, password == MockingBirdDBHelper.dummyUser().password else {
                completion(.Fail, false)
                return
            }
            /* We would normally pass in the user details to this method
               however for this dummy project we only have one user so the values will be set manually/hard coded
             */
            MockingBirdDBHelper.createUser()
            completion(.Success, true)
        }
    }
}


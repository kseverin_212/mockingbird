import Foundation
import SVProgressHUD

struct MBNetworkClient {

    func makeRequest(_ request:WebRequest, completionHandler: @escaping (APIResponseStatus, Data?)->Void){

        MBNetworkClient.showLoadingHud()

        let task = URLSession.shared.dataTask(with: request.url){ data, response , error in
            
            guard
                  let statusCode = (response as? HTTPURLResponse)?.statusCode,
                case 200...299 = statusCode else { completionHandler(.Fail, nil); return}

            MBNetworkClient.hideLoadingHud()
            completionHandler(.Success, data)
        }
        task.resume()
    }
}


extension MBNetworkClient {
    
    static func networkStatus(show: Bool) {
        DispatchQueue.main.async {  UIApplication.shared.isNetworkActivityIndicatorVisible = show }
    }
    
    static func showLoadingHud(withMessage message: String = "Loading") {
        networkStatus(show: true)
        SVProgressHUD.show(withStatus: message)
    }
    
    static func hideLoadingHud() {
        networkStatus(show: false)
        SVProgressHUD.dismiss()
    }
}

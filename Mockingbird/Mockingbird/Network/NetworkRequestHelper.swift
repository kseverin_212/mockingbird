import Foundation

typealias HTTPHeaders = [String: String]

protocol RequestBuilder {
    
    var httpTimeOut:Double { get }
    var headers:HTTPHeaders { get }
    var httpMethod:HTTPMethod { get }
}

extension RequestBuilder{
    
    var headers:HTTPHeaders{
        
        return [ "Content-Type": "application/json" ]
    }
    
    var httpTimeOut: Double {
        return 12.0
    }
    
    var httpMethod:HTTPMethod {
        return .GET
    }
}

struct WebRequest: RequestBuilder{
    
    typealias Parameters = [String: Any]

    var url: URL
    var parameters: Parameters?
    
    init(url:URL){
        self.url = url
        self.parameters = nil
    }
}

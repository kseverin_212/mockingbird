import XCTest

class MockingbirdUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    override func tearDown() {}

    func testInvalidLogin() {
        
        let user = "kerde@live.com"
        let pass = "test"
        
        let app = XCUIApplication()
        app.buttons["Get Started"].tap()
        
        let emailTxtField = app.textFields["Email Address"]
        XCTAssertTrue(emailTxtField.exists)
        emailTxtField.tap()
        emailTxtField.typeText(user)
        
        let passTxtField = app.secureTextFields["Password"]
        XCTAssertTrue(passTxtField.exists)
        passTxtField.tap()
        passTxtField.typeText(pass)
        
        app.buttons["Login"].tap()
        
        let erroAlert = app.alerts["Hmm"].buttons["Okay"]
        expectation(for: NSPredicate(format:"exists == 1"), evaluatedWith: erroAlert, handler: nil)
        waitForExpectations(timeout: 6, handler: nil)
        
        XCTAssertTrue(erroAlert.exists)
    }
    
    func testValidLogin() {
        
        let user = "kerde@test.com"
        let pass = "test"
        
        let app = XCUIApplication()
        app.buttons["Get Started"].tap()
        
        let emailTxtField = app.textFields["Email Address"]
        XCTAssertTrue(emailTxtField.exists)
        emailTxtField.tap()
        emailTxtField.typeText(user)
        
        let passTxtField = app.secureTextFields["Password"]
        XCTAssertTrue(passTxtField.exists)
        passTxtField.tap()
        passTxtField.typeText(pass)
        
        app.buttons["Login"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        let submitTweetBtn = elementsQuery.buttons["add"]

        expectation(for: NSPredicate(format:"exists == 1"), evaluatedWith: submitTweetBtn, handler: nil)
        waitForExpectations(timeout: 8, handler: nil)
        
        XCTAssertTrue(submitTweetBtn.exists)
    }
    
    func testMissingFields(){
        
        let app = XCUIApplication()
        app.buttons["Get Started"].tap()
        app.buttons["Login"].tap()
        let error = app.alerts["Woah"].buttons["Okay"]
        XCTAssertTrue(error.exists)
        error.tap()
    }
    
    func testInValidEmail(){
        
        let user = "kerde@"
        let pass = "test"
        
        let app = XCUIApplication()
        app.buttons["Get Started"].tap()
        let emailAddressTextField = app.textFields["Email Address"]
        emailAddressTextField.tap()
        emailAddressTextField.typeText(user)

        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(pass)
        
        let loginButton = app.buttons["Login"]
        loginButton.tap()
    
        let error = app.alerts["Oops"].buttons["Okay"]
        XCTAssertTrue(error.exists)
        error.tap()
    }
}
